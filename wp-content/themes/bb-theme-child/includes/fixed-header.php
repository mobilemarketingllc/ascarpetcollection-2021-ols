<header class="fl-page-header fl-page-header-fixed fl-page-nav-right">
	<div class="fl-page-header-wrap">
		<div class="fl-page-header-container container">
			<div class="fl-page-header-row row">
				<div class="fl-page-logo-wrap col-md-3 col-sm-12">
					<div class="fl-page-header-logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php FLTheme::logo(); ?></a>
					</div>
				</div>
				<div class="fl-page-fixed-nav-wrap col-md-9 col-sm-12">
					<div class="fl-page-nav-wrap">
						<nav class="fl-page-nav fl-nav navbar navbar-default" role="navigation">
							<div class="fl-page-nav-collapse collapse navbar-collapse">
								<?php 
								
								wp_nav_menu(array(
									'theme_location' => 'header',
									'items_wrap' => '<ul id="%1$s" class="nav navbar-nav navbar-right %2$s">%3$s</ul>',
									'container' => false,
									'fallback_cb' => 'FLTheme::nav_menu_fallback'
								)); 
								
								?>
							</div>
						</nav>
					</div>
				</div>
				<div class="fl-col fl-col-small fl-visible-desktop-medium" style="width: 8%;">
					<div class="fl-col-content fl-node-content">
						<div class="fl-module fl-module-html" data-animation-delay="0.0">
							<div class="fl-module-content fl-node-content">
								<div class="fl-html">
									<div class="fl-page-nav-search-wrap">
										<div class="fl-page-nav-search">
											<a href="javascript:void(0);" class="fa fa-search"></a>
											<form method="get" role="search" action="https://ascarpetcollection.com" title="Type and press Enter to search." style="display: none;">
												<input type="text" class="fl-search-input form-control" name="s" placeholder="Search" value="">
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header><!-- .fl-page-header-fixed -->