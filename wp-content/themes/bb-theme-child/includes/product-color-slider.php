<?php
global $post;
$flooringtype = $post->post_type; 
$brand = get_field('brand') ;

?>
<div class="product-variations">
    <h3>Color Variations</h3>
    <div class="row">
        <?php
		
			$value = get_post_meta($post->ID, 'collection', true);
			$key =  "collection";
		
        $args = array(
            'post_type'      => $flooringtype,
            'posts_per_page' => 10,
            'post_status'    => 'publish',
            'meta_query'     => array(
                array(
                    'key'     => $key,
                    'value'   => $value,
                    'compare' => '='
                )
            )
        );
        ?>
        <?php
        $the_query = new WP_Query( $args );
        ?>


        <div class="fr-slider color_variations_slider" data-fr='{"slidesToScroll":7,"slidesToShow":7,"arrows":true,"infinite": false}'>
            <!-- <a href="#" class="arrow prev"><i class="fa fa-angle-left"></i></a>
            <a href="#" class="arrow next"><i class="fa fa-angle-right"></i></a> -->
            <div class="slides">
                <?php  while ( $the_query->have_posts() ) {
                $the_query->the_post(); ?>
                    <div class="slide col-md-2 col-sm-3 col-xs-6 color-box">
                        <a  href="<?php the_permalink(); ?>">
                         
                              <?php
								if(get_field('swatch_image_link')){
	                              	$image = get_field('swatch_image_link'); 
									
									if(strpos($image , 's7.shawimg.com') !== false){
										if(strpos($image , 'http') === false){ 
											$image = "http://" . $image;
										}
									}else{
										if(strpos($image , 'http') === false){ 
											$image = "https://" . $image;
										}
									}
									$image= "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[222]&sink";
								}else{
									$image = "http://placehold.it/222x222?text=COMING+SOON";
								}
				                  $style= "padding: 5px;";
								?>
							
                                <img src="<?php echo $image; ?>" style="<?php echo $style; ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" />
                         



                        </a>
                        <br />
                        <small><?php the_field('color'); ?></small>
                    </div>
                <?php } ?>
            </div>
        </div>

    <?php wp_reset_postdata(); ?>


</div>
    </div>