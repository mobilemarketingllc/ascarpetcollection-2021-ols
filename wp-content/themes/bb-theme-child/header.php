<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php do_action('fl_head_open'); ?>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php FLTheme::title(); ?>
<?php FLTheme::favicon(); ?>
<?php FLTheme::fonts(); ?>
<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
<![endif]-->

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<?php 

wp_head(); 

FLTheme::head();

?>
<script type="text/javascript">
    var _ss = _ss || [];
    _ss.push(['_setDomain', 'https://koi-3QNH95Y2JS.marketingautomation.services/net']);
    _ss.push(['_setAccount', 'KOI-42L82RBHYA']);
    _ss.push(['_trackPageView']);
(function() {
    var ss = document.createElement('script');
    ss.type = 'text/javascript'; ss.async = true;
    ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QNH95Y2JS.marketingautomation.services/client/ss.js?ver=1.1.1';
    var scr = document.getElementsByTagName('script')[0];
    scr.parentNode.insertBefore(ss, scr);
})();
</script>

</head>

<body <?php body_class(); ?> itemscope="itemscope" itemtype="http://schema.org/WebPage">
<?php 
	
FLTheme::header_code();
	
do_action('fl_body_open'); 

?>
<div class="fl-page">
	<?php
	
	do_action('fl_page_open');
	
	FLTheme::fixed_header();
	
	do_action('fl_before_top_bar');
	
	FLTheme::top_bar();

	echo do_shortcode('[fl_builder_insert_layout slug="covid-custom-topbar"]');
    echo do_shortcode('[fl_builder_insert_layout slug="top-bar-1"]');
	
	do_action('fl_after_top_bar');
	do_action('fl_before_header');
	
	FLTheme::header_layout();
	
	do_action('fl_after_header');
	do_action('fl_before_content');
	
	?>
	<div class="fl-page-content" itemprop="mainContentOfPage">
	
		<?php do_action('fl_content_open'); ?>