<div class="product-grid swatch" itemscope itemtype="http://schema.org/CollectionPage">
	<div class="row product-row">
		<?php while ( have_posts() ): the_post();
		//collection field
 $collection = get_field('collection', $post->ID);
		?>
		<div class="col-md-3 col-sm-4 col-xs-6">
			<!-- <div class="fl-post-grid-post" itemscope itemtype="<?php //FLPostGridModule::schema_itemtype(); ?>"> -->
			<div class="fl-post-grid-post" itemscope itemtype="Product">
				<?php FLPostGridModule::schema_meta(); ?>
				<?php if(get_field('swatch_image_link')) { ?>
				<div class="fl-post-grid-image prod_img">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<?php
							$itemImage = get_field('swatch_image_link');
														  
							if(strpos($itemImage , 's7.shawimg.com') !== false){
								if(strpos($itemImage , 'http') === false){ 
									$itemImage = "http://" . $itemImage;
								}
								$class = "";
							}else{
								if(strpos($itemImage , 'http') === false){ 
									$itemImage = "https://" . $itemImage;
								}
								$class = "shadow";
							}							  
							$image= "https://mobilem.liquifire.com/mobilem?source=url[".$itemImage . "]&scale=size[222]&sink";
							
						?>
						<img class="<?php echo $class; ?>" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
                        <?php
						// exclusive icon condition
						if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {    ?>
						<span class="exlusive-badge"><img src="<?php echo plugins_url( '/grand-child/product-listing-templates/images/exclusive-icon.png');?>" alt="<?php the_title(); ?>" /></span>
						<?php } ?>
						
					</a>
				</div>
				<?php } else { ?>
				<div class="fl-post-grid-image ">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<?php //the_post_thumbnail($settings->image_size); ?>
						<img class="<?php echo $class; ?>" src="http://placehold.it/168x123?text=COMING+SOON" alt="<?php the_title_attribute(); ?>" />
					</a>
				</div>

				<?php } ?>
				<div class="fl-post-grid-text product-grid btn-grey">
					<h4><?php the_field('collection'); ?></h4>
					<h2 class="fl-post-grid-title" itemprop="headline">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php //the_title(); ?><?php the_field('color'); ?></a>
					</h2>
					<a href="<?php echo site_url(); ?>/flooring-coupon/?keyword=<?php echo @$_COOKIE['keyword']; ?>&brand=<?php echo get_field('manufacturer');?>" target="_self" class="fl-button" role="button">
						<span class="fl-button-text">GET COUPON</span>
					</a><br />
					<a class="link" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
	</div>
</div>