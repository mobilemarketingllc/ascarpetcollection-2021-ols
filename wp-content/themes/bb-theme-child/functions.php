<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );
// require_once 'product-addon/grand-child.php';
//$500 off coupon sharpspring form code
add_action( 'gform_after_submission_6', 'post_to_third_party_6', 10, 2 );

function post_to_third_party_6( $entry, $form ) {
    $baseURI = 'https://app-3QNH95Y2JS.marketingautomation.services/webforms/receivePostback/MzawMDE3MjO1AAA/';
    $endpoint = '29e8e22b-9558-4547-9197-b94fab84b698';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '7' ),
        'last' => rgar( $entry, '8' ),
        'email' => rgar( $entry, '1' ),
        'phone' => rgar( $entry, '2' ),
        'opt in' => rgar( $entry, '5' ),
        'terms' => rgar( $entry, '10' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}
//contact us form code sharpspring
add_action( 'gform_after_submission_5', 'post_to_third_party_5', 10, 2 );

function post_to_third_party_5( $entry, $form ) {
    $baseURI = 'https://app-3QNH95Y2JS.marketingautomation.services/webforms/receivePostback/MzawMDE3MjO1AAA/';
    $endpoint = 'fc02dff7-1f1a-4bbd-bd37-0d9c5ac3870b';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'phone' => rgar( $entry, '4' ),
        'interested in' => rgar( $entry, '9' ),
        'questions comments' => rgar( $entry, '10' ),
        'terms conditions' => rgar( $entry, '11' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

//In-home estimate form code sharpspring
add_action( 'gform_after_submission_2', 'post_to_third_party_2', 10, 2 );

function post_to_third_party_2( $entry, $form ) {
    $baseURI = 'https://app-3QNH95Y2JS.marketingautomation.services/webforms/receivePostback/MzawMDE3MjO1AAA/';
    $endpoint = '73b3e80b-405e-4865-aec9-36c35667c172';
    $post_url = $baseURI . $endpoint;

    $field_id = 11; $field = GFFormsModel::get_field( $form, $field_id );
    $field_value_11 = is_object( $field ) ? $field->get_value_export( $entry ) : '';

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'phone' => rgar( $entry, '4' ),
        'street address' => rgar( $entry, '12' ),
        'city' => rgar( $entry, '13' ),
        'service required' => rgar( $entry, '6' ),
        'preferred day' => rgar( $entry, '7' ),
        'preferred time' => rgar( $entry, '8' ),
        'product interest' => rgar( $entry, '9' ),
        'project details' => rgar( $entry, '10' ),
        'terms conditions' => $field_value_11,
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

//Shop at home form code sharpspring
add_action( 'gform_after_submission_3', 'post_to_third_party_3', 10, 2 );

function post_to_third_party_3( $entry, $form ) {
    $baseURI = 'https://app-3QNH95Y2JS.marketingautomation.services/webforms/receivePostback/MzawMDE3MjO1AAA/';
    $endpoint = 'ae7ddcfb-768b-424b-94af-a9862eb7e6a9';
    $post_url = $baseURI . $endpoint;

    $field_id = 11; $field = GFFormsModel::get_field( $form, $field_id );
    $field_value_11 = is_object( $field ) ? $field->get_value_export( $entry ) : '';

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'phone' => rgar( $entry, '4' ),
        'service interest' => rgar( $entry, '6' ),
        'preferred day' => rgar( $entry, '7' ),
        'preferred time' => rgar( $entry, '8' ),
        'product interest' => rgar( $entry, '9' ),
        'project details' => rgar( $entry, '10' ),
        'terms conditions' => $field_value_11,
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
  wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
  wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
  wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
  wp_enqueue_script("cookie", get_stylesheet_directory_uri()."/resources/jquery.cookie.js", "", "", 1);
});

 
// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'gallery-menu' => __( 'Gallery Menu' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');




// Facetwp results
add_filter( 'facetwp_result_count', function( $output, $params ) {
    //$output = $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' results';
    $output =  $params['total'] . ' Products';
    return $output;
}, 10, 2 );
// Facetwp results pager
// function my_facetwp_pager_html( $output, $params ) {
//     $output = '';
//     $page = $params['page'];
//     $total_pages = $params['total_pages'];
//     if ( $page > 1 ) {
//         $output .= '<a class="facetwp-page" data-page="' . ($page - 1) . '"><span class="pager-arrow"><</span></a>';
//     }
//     $output .= '<span class="pager-text">page ' . $page . ' of ' . $total_pages . '</span>';
//     if ( $page < $total_pages && $total_pages > 1 ) {
//         $output .= '<a class="facetwp-page" data-page="' . ($page + 1) . '"><span class="pager-arrow">></span></a>';
//     }
//     return $output;
// }

// add_filter( 'facetwp_pager_html', 'my_facetwp_pager_html', 10, 2 );



// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

remove_action( 'wp_head', 'feed_links_extra', 3 );

if(@$_GET['keyword'] != '' && @$_GET['brand'] !="")
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
  	    setcookie('keyword' , @$_GET['keyword']);
	    setcookie('brand' , @$_GET['brand']);
	    wp_redirect( $url[0] );
	    exit;
 } 
 else if(@$_GET['brand'] !="" && @$_GET['keyword'] == '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('brand' , @$_GET['brand']);
        wp_redirect( $url[0] );
        exit;
 }
 else if(@$_GET['brand'] =="" && @$_GET['keyword'] != '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('keyword' , @$_GET['keyword']);
        wp_redirect( $url[0] );
        exit;
 }
 
 
// shortcode to show H1 google keyword fields
function new_google_keyword() 
{
      
   if( @$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "")
   {
        // return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring<h1>';
        return $google_keyword = '<h1 class="googlekeyword">SAVE UP TO $500 ON FLOORING*<h1>';
   }
   else
   {
       $keyword = @$_COOKIE['keyword'];
	   $brand = @$_COOKIE['brand'];
    //    return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
    return $google_keyword = '<h1 class="googlekeyword">SAVE UP TO $500 ON '.$brand.' '.$keyword.'<h1>';
   }
}
  add_shortcode('google_keyword_code', 'new_google_keyword');
  add_action('wp_head','cookie_gravityform_js');

  function cookie_gravityform_js()
  { // break out of php 
  ?>
  <script>
	  var brand_val ='<?php echo @$_COOKIE['brand'];?>';
	  var keyword_val = '<?php echo @$_COOKIE['keyword'];?>';  

      jQuery(document).ready(function($) {
      jQuery("#input_6_11").val(keyword_val);
      jQuery("#input_6_12").val(brand_val);
    });
  </script>
  <?php  
     setcookie('keyword' , '',-3600); 
     setcookie('brand' , '',-3600);
  
}

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;   
			   font-size:2.5em !important;
		   }
      </style>  
   <?php    
}
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong class="facet_titles">'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

// wp_clear_scheduled_hook( 'action_scheduler_run_queue' );

// function asdds_disable_default_runner() {
//    if ( class_exists( 'ActionScheduler' ) ) {
//        remove_action( 'action_scheduler_run_queue', array( ActionScheduler::runner(), 'run' ) );
//    }
// }
// // ActionScheduler_QueueRunner::init() is attached to 'init' with priority 1, so we need to run after that
// add_action( 'init', 'asdds_disable_default_runner', 10 );

if ( SITECOOKIEPATH != COOKIEPATH ) {
    setcookie(TEST_COOKIE, 'WP Cookie check', 0, SITECOOKIEPATH, COOKIE_DOMAIN);
    }

    //add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}

add_filter( 'auto_update_plugin', '__return_false' );

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_1' );

function wpse_100012_override_yoast_breadcrumb_trail_1( $links ) {
    global $post;
    if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/luxury-vinyl/',
            'text' => 'Luxury Vinyl Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/luxury-vinyl/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    return $links;
}