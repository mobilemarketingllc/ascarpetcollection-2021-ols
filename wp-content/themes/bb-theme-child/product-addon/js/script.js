var $ = jQuery;

function reloadFacet() {
    $('.facetwp-facet').each(function () {
        if ($(this).children('.facet-inner').length == 0) {
            var moreCount = $(this).children('.facetwp-overflow').children('.facetwp-checkbox').length;
            $(this).children('div').wrapAll('<div class="facet-inner" />');
            $(this).children('.facetwp-toggle:eq(0)').text("See " + moreCount + " more");
        }
    });
}
// $(document).on('facetwp-refresh', function () {
//     reloadFacet();
//     if (FWP.loaded) {
//         $('.facetwp-facet .facet-inner').hide().before('<div class="facetwp-loading" />').nextAll('.facetwp-toggle').hide();
//     }
// });
$(document).on('facetwp-loaded', function () {
    $.each(FWP.settings.num_choices, function (key, val) { var html = $('.facetwp-facet-' + key).html(); if (html == "") { $('.facetwp-facet-' + key).parent().hide() } else { $('.facetwp-facet-' + key).parent().show(); } });
    reloadFacet();
});
reloadFacet();

function fr_slider_init(ob) {
    $(ob).each(function () {
        var default_settings = { slidesToScroll: 1, slidesToShow: 1 }
        var settings = fr_parse_attr_data($(this).attr("data-fr"));
        settings = $.extend(default_settings, settings);
        settings = fr_apply_filter("fr_slider_init", settings, [$(ob)]);
        $(ob).find(".slides").slick(settings);
        $(ob).find(".slides").on('beforeChange', function (event, slick, currentSlide, nextSlide) { next_slide(this, nextSlide); });

        function next_slide(ob, nextSlide) {
            if (nextSlide == 0) { $(ob).parents(".fr-slider").first().find(".arrow.prev").animate({ opacity: 0 }); } else { $(ob).parents(".fr-slider").first().find(".arrow.prev").animate({ opacity: 1 }); }
            if (nextSlide >= $(ob).find(".slide").length - settings.slidesToScroll) { $(ob).parents(".fr-slider").first().find(".arrow.next").animate({ opacity: 0 }); } else { $(ob).parents(".fr-slider").first().find(".arrow.next").animate({ opacity: 1 }); }
        }
        next_slide($(ob).find(".slides"), 0);
        $(ob).addClass("init");
    });
}
$(document).ready(function () {
    $('.onlycharallow input[type="text"]').attr('onKeyPress','return validateChar(event)');
    $('.onlynumallow input').attr('onKeyPress','return validateNum(event)');
    $('textarea').attr('onkeydown','return ValidateTextarea(event)');

    $('#product-images-holder').lightGallery();
    $('.fl-callout .fl-callout-title').each(function () {
        $(this).next('.fl-callout-text-wrap').andSelf().wrapAll('<div class="fl-callout-content-wrap"/>');
    });

    var windowWidth = $(window).width();
    if ($('.toggle-image-thumbnails > div').length > 6 && $('.toggle-image-thumbnails').hasClass('vertical-slider') && windowWidth >= 769) {
        $('.toggle-image-thumbnails').slick({
            vertical: true,
            slidesToScroll: 6,
            slidesToShow: 6,
            arrows: true,
            infinite: false,
            mobileFirst: false,
            prevArrow: '<a href="javascript:void(0)" class="arrow prev"><i class="fa fa-angle-up"></i></a>',
            nextArrow: '<a href="javascript:void(0)" class="arrow next"><i class="fa fa-angle-down"></i></a>',
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 6
                    }
                }
            ]
        });
    }
    else {
        if ($('.toggle-image-thumbnails > div').length > 6 && $('.toggle-image-thumbnails').hasClass('vertical-slider')) {
            $('.toggle-image-thumbnails').slick({
                vertical: false,
                slidesToScroll: 5,
                slidesToShow: 5,
                arrows: true,
                infinite: false,
                mobileFirst: false,
                prevArrow: '<a href="javascript:void(0)" class="arrow prev"><i class="fa fa-angle-left"></i></a>',
                nextArrow: '<a href="javascript:void(0)" class="arrow next"><i class="fa fa-angle-right"></i></a>',
                responsive: [
                    {
                        breakpoint: 580,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                        }
                    },
                    {
                        breakpoint: 420,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }
                ]
            });
        }
    }

    if ($('.toggle-image-thumbnails > div').length > 7) {
        $('.toggle-image-thumbnails').slick({
            slidesToScroll: 7,
            slidesToShow: 7,
            arrows: true,
            infinite: false,
            mobileFirst: false,
            prevArrow: '<a href="javascript:void(0)" class="arrow prev"><i class="fa fa-angle-left"></i></a>',
            nextArrow: '<a href="javascript:void(0)" class="arrow next"><i class="fa fa-angle-right"></i></a>',
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 7,
                        slidesToScroll: 7
                    }
                },
                {
                    breakpoint: 680,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 580,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        });
    }

    $('.toggle-image-thumbnails .toggle-image-holder a').click(function (e) {
        e.preventDefault();
        $('.toggle-image-thumbnails .toggle-image-holder a').removeClass('active');
        url = $(this).attr("data-background");
        bkImg = "url('" + url + "')";
        $($(this).attr("data-fr-replace-bg")).css("background-image", bkImg).attr("data-responsive", url).attr("data-src", url);
        $($(this).attr("data-fr-replace-bg") + ' a.popup-overlay-link').attr('data-src', url);
        $(this).addClass('active');
    });

    $('.color_variations_slider > .slides').slick({
        dots: false,
        infinite: false,
        speed: 300,
        arrows: true,
        slidesToShow: 7,
        slidesToScroll: 7,
        mobileFirst: false,
        prevArrow: '<a href="javascript:void(0)" class="arrow slick-prev">Previous</a>',
        nextArrow: '<a href="javascript:void(0)" class="arrow slick-next">Next</a>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    
    if($('.facet_filters .fl-html > .close_bar').length==0){
        $('.facet_filters .fl-html').prepend('<div class="close_bar"><a class="close close_sidebar" href="javascript:void(0)"><i class="fa fa-close">&nbsp;</i></a></div>');
    }
    if($('.facet_filters .fl-html > .close_sidebar_button').length==0){
        $('.facet_filters .fl-html').append('<div class="close_sidebar_button"><a class="fl-button close_sidebar" href="#"><span class="fl-button-text">View Results</span></a></div>');
    }
});
$(window).ready(function () {

    fr_add_filter("fr_slider_init", function (settings, slider) {
        if ($(window).width() < 726) {
            if (slider.is(".color_variations_slider")) {
                settings.slidesToScroll = 3;
                settings.slidesToShow = 3;
            }
        }
        return settings;
    });
    if ($().slick) {
        fr_slider_init(".fr-slider");
        $("body").on("click", ".fr-slider .prev", function (e) {
            e.preventDefault();
            $(this).parents(".fr-slider").first().find(".slides").slick('slickPrev');
        });
        $("body").on("click", ".fr-slider .next", function (e) {
            e.preventDefault();
            $(this).parents(".fr-slider").first().find(".slides").slick('slickNext');
        });
    }

    function fr_slider_delete(ob) { $(ob).each(function () { $(this).find(".slides").slick('unslick'); }); }
    $("body").on("click", "[data-fr-replace-src]", function (e) {
        e.preventDefault();
        url = $(this).attr("data-src");
        bkImg = "url('" + url + "')";
        $($(this).attr("data-fr-replace-src")).attr("src", bkImg);
    });
    $("body").on("click", "[data-fr-replace-bg]", function (e) {
        e.preventDefault();
        url = $(this).attr("data-background");
        bkImg = "url('" + url + "')";
        $($(this).attr("data-fr-replace-bg")).css("background-image", bkImg);
    });
    $(".fr_toggle_box .box_content").each(function () {
        $(this).show();
        var dir = $(this).parents(".fr_toggle_box").first().data("dir");
        var size = $(this).outerHeight();
        if (dir == "left" || dir == "right") { size = $(this).outerWidth(); }
        $(this).css(dir, -size);
    });
    $(".fr_toggle_box .handle").click(function () {
        var parent = $(this).parents(".fr_toggle_box").first();
        var content = parent.find(".box_content").first();
        var dir = parent.data("dir");
        if (!dir) { dir = "bottom"; }
        data = {};
        if (parent.is(".active")) {
            var size = content.outerHeight();
            if (dir == "left" || dir == "right") { size = content.outerWidth(); }
            data[dir] = -size;
            content.stop().animate(data);
            parent.removeClass("active");
            parent.find(".bg").fadeOut();
        } else {
            data[dir] = 0;
            content.stop().animate(data);
            parent.addClass("active");
            parent.find(".bg").fadeIn();
        }
    });
    $(window).load(function () {
        setTimeout(function () {
            if (!$.cookie('fr_toggle_box_opened')) {
                $(".fr_toggle_box .handle").click();
                setTimeout(function () { if ($(".fr_toggle_box").is(".active")) { $(".fr_toggle_box .handle").click(); } }, 7000);
                $.cookie('fr_toggle_box_opened', 1, { expires: 365, path: '/' });
            }
        }, 1000);
    });
    var fr_link_dont_redirect = 0;
    $("body").on("click", "[data-fr-link] a", function (e) { fr_link_dont_redirect++; });
    $("body").on("click", "[data-fr-link]", function (e) { if (fr_link_dont_redirect <= 0) { window.location = $(this).attr("data-fr-link"); return false; } else { fr_link_dont_redirect--; } });
    $(".slider-menu").each(function () { $(this).height($(this).parent().height()); });
    fr_click_outside(".slider-menu", ".slider-menu", function (ob) { return $(ob).is(".active"); }, function (ob) { if ($(ob).is(".active")) { close_slider_menu(ob); } else { open_slider_menu(ob); } });
    $(".slider-menu").find(".icon").click(function () { if (!$(this).parents(".slider-menu").is(".active")) { open_slider_menu($(this).parents(".slider-menu")); } else { close_slider_menu($(this).parents(".slider-menu")); } });

    function open_slider_menu(ob) {
        $(ob).addClass("active");
        $(ob).find("ul").stop().animate({ "margin-right": 0 });
    }

    function close_slider_menu(ob) {
        $(ob).removeClass("active");
        $(ob).find("ul").stop().animate({ "margin-right": -$(ob).outerWidth() + $(ob).find(".icon").first().outerWidth() });
    }
    $("body").on("click", "a[href^='#']", function (e) { var target = $(this).attr("href"); if ($(target).length && $(target).is(".fr_popup")) { e.preventDefault(); if (!$(target).is(".active")) { fr_open_popup(target); } } });
    $("body").on("click", ".fr_popup .close_popup", function (e) { e.preventDefault(); var target = $(this).parents(".fr_popup"); if ($(target).is(".active")) { fr_close_popup(target); } });

    function fr_open_popup(target) {
        $(target).show();
        fr_center_in_window($(target).find(".content"));
        $(target).addClass("active");
    }

    function fr_close_popup(target) {
        $(target).removeClass("active");
        $(target).hide();
    }

    function fr_center_in_window(ob, offset) {
        if (!offset) { offset = 0; }
        var wt = $(window).scrollTop();
        var wh = $(window).height();
        var oh = $(ob).outerHeight();
        console.log(wh, oh, wt, offset);
        $(ob).css("top", (wh - oh) / 2 + wt + offset);
    }

    function close_modal() {
        $(".active-gallery-modal").fadeOut(200);
        setTimeout(function () {
            $("body").removeClass("modal-open");
        }, 300);
    }

    function open_modal(html) {
        var header = $(".fl-page-header").outerHeight();
        if (!$(".fl-page-header").is(":visible")) { header = $("#djcustom-header").outerHeight(); }

        $("body").addClass("modal-open");
        $(".active-gallery-modal").html(html);

        $(".active-gallery-modal").fadeIn();
        var id = $(html).find(".gallerySlider").attr("id");

        setTimeout(opensider, 100, "#" + id);
        function opensider(id) {
            $(id).slick({
                speed: 500,
                fade: true,
                cssEase: 'linear',
                lazyLoad: 'ondemand',
                nextArrow: '<i class="fas fa-chevron-left prev"></i>',
                prevArrow: '<i class="fas fa-chevron-right next"></i>',
            });
        }
    }

    $("body").on("click", ".open-gallery-modal", function (e) {
        var html = $(this).find(".gallery-modal").html();
        if ($(".active-gallery-modal").length == 0) {
            $(".fl-page-content").prepend("<div class='active-gallery-modal'></div>");
            open_modal(html);
        }
        else {
            if (!$(".active-gallery-modal").is(":visible")) {
                open_modal(html);
            } else {
                close_modal();
            }
        }
        e.preventDefault();
    });
    $("body").on("click", ".active-gallery-modal .close_modal", function (e) {
        e.preventDefault();
        close_modal();
    });
    //Close Gallery Popup on Escape Key
    $(document).keyup(function (e) {
        if (e.keyCode === 27) {
            close_modal();
        }
    });

    var hash = window.location.hash.substr(1);
    $(".fl-tabs-label").each(function () {
        if ($.trim($(this).text()).toLowerCase() == hash.toLowerCase()) {
            //console.log($(this));
            change_tab($(this));
            return false;
        }
    });
    $("footer .menu-item-has-children").click(function (e) {
        if ($(window).width() <= 768) {
            e.preventDefault();
            $(this).find(">ul").toggle();
        }
    });
});

function fr_click_outside(excluded_element, element_to_hide, cond_func, hide_func) { $(document).click(function (event) { if (!$(event.target).closest(excluded_element).length) { if ((cond_func && cond_func(element_to_hide)) || (!cond_func && $(element_to_hide).is(":visible"))) { if (hide_func) { hide_func(element_to_hide); } else { $(element_to_hide).hide(); } } } }); }

function change_tab(ob) {
    ob.parent().find(".fl-tab-active").removeClass("fl-tab-active");
    ob.addClass("fl-tab-active");
    var index = ob.index();
    ob.parents(".fl-tabs").first().find(".fl-tabs-panel .fl-tabs-label").removeClass("fl-tab-active");
    ob.parents(".fl-tabs").first().find(".fl-tabs-panel .fl-tabs-panel-content").removeClass("fl-tab-active");
    ob.parents(".fl-tabs").first().find(".fl-tabs-panel:eq(" + index + ") .fl-tabs-panel-content").addClass("fl-tab-active");
}

function fr_parse_attr_data(data) {
    if (!data) { data = ""; }
    if (data.substr(0, 1) != "{") { data = "{" + data + "}"; }
    return $.parseJSON(data);
}
var fr_filters = [];

function fr_add_filter(filter, func) { filter = filter; }

function fr_apply_filter(filter, res, args) {
    if (typeof fr_filters[filter] != "undefined") {
        for (k in fr_filters[filter]) {
            var args2 = [res].concat(args);
            res = fr_filters[filter][k].apply(null, args2);
        }
    }
    return res;
}

// Validation Scripts
function validateChar(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 123)) {
        if (charCode == 8 || charCode == 32 || charCode == 9){
            console.log(charCode);
            return true;
        }
        else{
            return false;
        }
    } else{
        return true;
    }
}
function validateNum(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        if (charCode == 43 || charCode == 40 || charCode == 41 || charCode == 9){
            return true;
        }
        else{
            return false;
        }
    } else{
        return true;
    }
}

function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (false);
    }                    
    return (true);
}

function validateFloat(evt)
{
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        return (false);
    }
    return (true);
}