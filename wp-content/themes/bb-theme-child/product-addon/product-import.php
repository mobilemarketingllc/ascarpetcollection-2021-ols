<?php

//require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';
require_once( ABSPATH . "wp-includes/wp-db.php" );
require_once plugin_dir_path( __FILE__ ) . 'class-logger.php';

class Main_Processing {

	use Product_Import;
	/**
	 * @var WP_Example_Request
	 */
	protected $process_single;

	/**
	 * @var WP_Example_Process
	 */
	protected $process_all;

	/**
	 * Example_Background_Processing constructor.
	 */
	public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'init' ) );
		add_action( 'admin_bar_menu', array( $this, 'admin_bar' ), 100 );
		add_action( 'init', array( $this, 'process_handler' ) );

		
	}

	/**
	 * Init
	 */
	public function init() {
		
		
		require_once plugin_dir_path( __FILE__ ) . 'class-logger.php';
		require_once plugin_dir_path( __FILE__ ) . 'async-requests/class-example-request.php';
		require_once plugin_dir_path( __FILE__ ) . 'background-processes/class-example-process.php';

		$this->process_single = new WP_Example_Request();
		$this->process_all    = new WP_Example_Process();
	}

	/**
	 * Admin bar
	 *
	 * @param WP_Admin_Bar $wp_admin_bar
	 */
	public function admin_bar( $wp_admin_bar ) {
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}
		
		// $wp_admin_bar->add_menu( array(
		// 	'id'    => 'grand-child',
		// 	'title' => __( 'Products', 'grand-child' ),
		// 	'href'  => '#',
		// ) );

		// $wp_admin_bar->add_menu( array(
		// 	'parent' => 'grand-child',
		// 	'id'     => 'grand-child-single',
		// 	'title'  => __( 'Single Category', 'grand-child' ),
		// 	'href'   => wp_nonce_url( admin_url( '?process=single'), 'process' ),
		// ) );

		// $wp_admin_bar->add_menu( array(
		// 	'parent' => 'grand-child',
		// 	'id'     => 'grand-child-all',
		// 	'title'  => __( 'All Category', 'grand-child' ),
		// 	'href'   => wp_nonce_url( admin_url( '?process=all'), 'process' ),
		// ) );
	}

	/**
	 * Process handler
	 */
	public function process_handler() {
		if ( ! isset( $_GET['process'] ) ) {

			return;
		}

		
		if ( 'single' === $_GET['process'] ) {
			$this->handle_single();
		}

		if ( 'all' === $_GET['process'] ) {
			
			$this->handle_all();
		}
	}

	/**
	 * Handle single
	 */
	protected function handle_single() {
		$names = $this->get_names();
		$rand  = array_rand( $names, 1 );
		$name  = $names[ $rand ];

		$this->process_single->data( array( 'name' => $name ) )->dispatch();
	}

	/**
	 * Handle all
	 */
	protected function handle_all() {

		
		global $wpdb;	
		
		$file_name = $_GET['main_category'].'_'.$_GET['product_brand'].'.csv';		
		$upload_dir = wp_upload_dir(); 
		$array = $fields = array(); $i = 0;
		$handle = @fopen($upload_dir['basedir'].'/'.'sfn-data/'.$file_name, "r");
		$allValues ="";
		$_session['sfn_post_type'] = $_GET['main_category'];
		$cnt =0;
		$batch=[];
			if ($handle) {
				
				while (($row = fgetcsv($handle, 20096)) !== false) {
					if (empty($fields)) {
						$fields = $row;
						continue;
					}
					
					
				 	foreach ($row as $k=>$value) {
						$array[$fields[$k]] = $value;
					}
 
					$values = "";
					$values = $this->insert_product($array,$_GET['main_category']);
					$allValues .= $values;
					if($cnt == 500){
						$allValues =	substr_replace($allValues, "", -1);
						$wpdb->query("INSERT INTO {$wpdb->postmeta} (post_id,meta_key,meta_value) VALUES {$allValues}");	
						$cnt =0;
						$allValues ="";
					}
					
					$i++;
					$cnt++;
				}
				if (!feof($handle)) {
					echo "Error: unexpected fgets() fail\n";
				}



				fclose($handle);
			}
			
			if($allValues){
				$allValues =	substr_replace($allValues, "", -1);
				$wpdb->query("INSERT INTO {$wpdb->postmeta} (post_id,meta_key,meta_value) VALUES {$allValues}");
			}
			
			
		
	}

	/**
	 * Get names
	 *
	 * @return array
	 */
	protected function get_names() {
		return array(
			'Grant Buel',
			'Bryon Pennywell',
			'Jarred Mccuiston',
			'Reynaldo Azcona',
			'Jarrett Pelc',
			'Blake Terrill',
			'Romeo Tiernan',
			'Marion Buckle',
			'Theodore Barley',
			'Carmine Hopple',
			'Suzi Rodrique',
			'Fran Velez',
			'Sherly Bolten',
			'Rossana Ohalloran',
			'Sonya Water',
			'Marget Bejarano',
			'Leslee Mans',
			'Fernanda Eldred',
			'Terina Calvo',
			'Dawn Partridge',
		);
	}

}

