<?php get_header(); ?>
<!-- <div class="container">
    <div class="row">
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/" style="padding-top: 20px; padding-left: 20px;">
      <a href="/">Home</a> &raquo; <a href="/flooring/">Flooring</a> &raquo; <a href="/flooring/luxury-vinyl/">Luxury Vinyl</a> &raquo; <a href="/flooring/luxury-vinyl/products/">Luxury Vinyl products</a> &raquo; <?php the_title(); ?>
    </div>
        </div>
</div> -->

<div class="container">
	<div class="row">
		<div class="fl-content product<?php //FLTheme::content_class(); ?>">
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				<?php 
			 
				get_template_part('content', 'single-product'); ?>
			<?php endwhile; endif; ?>
		</div>
		<?php //FLTheme::sidebar('right'); ?>
	</div>
</div>

<?php get_footer(); ?>